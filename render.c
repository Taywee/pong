#include "registers.h"

struct Sprite {
    unsigned char y;
    unsigned char tile;
    unsigned char attributes;
    unsigned char x;
};

extern unsigned char ballx;
#pragma zpsym ("ballx");
extern unsigned char bally;
#pragma zpsym ("bally");
extern unsigned char paddle1y;
#pragma zpsym ("paddle1y");
extern unsigned char paddle2y;
#pragma zpsym ("paddle2y");
extern unsigned char score1;
#pragma zpsym ("score1");
extern unsigned char score2;
#pragma zpsym ("score2");

extern struct Sprite ball_sprite;
extern struct Sprite player1_paddle_sprites[3];
extern struct Sprite player2_paddle_sprites[3];

extern void UpdateSprites(void) {
    ball_sprite.y = bally;
    ball_sprite.x = ballx;
    player1_paddle_sprites[0].y = paddle1y;
    player1_paddle_sprites[1].y = paddle1y + 0x08;
    player1_paddle_sprites[2].y = paddle1y + 0x10;
    player2_paddle_sprites[0].y = paddle2y;
    player2_paddle_sprites[1].y = paddle2y + 0x08;
    player2_paddle_sprites[2].y = paddle2y + 0x10;
}
#define NUMTILE(number) number + 0x2A

extern void DrawScore(void) {
    // Write to the right side of the screen
    SET(PPUADDR, 0x20);
    SET(PPUADDR, 0x5C);
    SET(PPUDATA, NUMTILE(score2));

    // Write to the left side of the screen
    SET(PPUADDR, 0x20);
    SET(PPUADDR, 0x42);
    SET(PPUDATA, NUMTILE(score1));
}
