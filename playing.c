#define RIGHTWALL 0xF4
#define TOPWALL 0x20
#define BOTTOMWALL 0xE0
#define LEFTWALL 0x08

#define BUTTON_A 0x80
#define BUTTON_B 0x40
#define BUTTON_SELECT 0x20
#define BUTTON_START 0x10
#define BUTTON_UP 0x08
#define BUTTON_DOWN 0x04
#define BUTTON_LEFT 0x02
#define BUTTON_RIGHT 0x01
#define PRESSED(buttons, button) (buttons & button) != 0

#define A_PRESSED(buttons) PRESSED(buttons, BUTTON_A)
#define B_PRESSED(buttons) PRESSED(buttons, BUTTON_B)
#define START_PRESSED(buttons) PRESSED(buttons, BUTTON_START)
#define SELECT_PRESSED(buttons) PRESSED(buttons, BUTTON_SELECT)
#define UP_PRESSED(buttons) PRESSED(buttons, BUTTON_UP)
#define DOWN_PRESSED(buttons) PRESSED(buttons, BUTTON_DOWN)
#define LEFT_PRESSED(buttons) PRESSED(buttons, BUTTON_LEFT)
#define RIGHT_PRESSED(buttons) PRESSED(buttons, BUTTON_RIGHT)

extern unsigned char ballright;
#pragma zpsym ("ballright");
extern unsigned char ballx;
#pragma zpsym ("ballx");
extern unsigned char ballspeedx;
#pragma zpsym ("ballspeedx");
extern unsigned char bally;
#pragma zpsym ("bally");
extern unsigned char ballspeedy;
#pragma zpsym ("ballspeedy");
extern unsigned char ballup;
#pragma zpsym ("ballup");
extern unsigned char paddle1y;
#pragma zpsym ("paddle1y");
extern unsigned char buttons1;
#pragma zpsym ("buttons1");
extern unsigned char paddlespeed;
#pragma zpsym ("paddlespeed");
extern unsigned char paddleaspeed;
#pragma zpsym ("paddleaspeed");
extern unsigned char score1;
#pragma zpsym ("score1");
extern unsigned char score2;
#pragma zpsym ("score2");
extern unsigned char paddle2y;
#pragma zpsym ("paddle2y");
extern unsigned char buttons2;
#pragma zpsym ("buttons2");
extern unsigned char ballbasespeed;
#pragma zpsym ("ballbasespeed");
extern unsigned char hitcount;
#pragma zpsym ("hitcount");

void EnginePlaying(void) {
    // Move Ball
    if (ballright) {
        ballx += ballspeedx;
    } else {
        ballx -= ballspeedx;
    }
    if (ballup) {
        bally -= ballspeedy;
    } else {
        bally += ballspeedy;
    }

    // Check bounds
    if (ballx > RIGHTWALL) {
        ballright = 0;
    } else if (ballx < LEFTWALL) {
        ballright = 1;
    }
    if (bally > BOTTOMWALL) {
        ballup = 1;
    } else if (bally < TOPWALL) {
        ballup = 0;
    }

    // Move Paddles
    if (UP_PRESSED(buttons1)) {
        if (A_PRESSED(buttons1)) {
            paddle1y -= paddleaspeed;
        } else {
            paddle1y -= paddlespeed;
        }
    }
    if (DOWN_PRESSED(buttons1)) {
        if (A_PRESSED(buttons1)) {
            paddle1y += paddleaspeed;
        } else {
            paddle1y += paddlespeed;
        }
    }
    if (UP_PRESSED(buttons2)) {
        if (A_PRESSED(buttons2)) {
            paddle2y -= paddleaspeed;
        } else {
            paddle2y -= paddlespeed;
        }
    }
    if (DOWN_PRESSED(buttons2)) {
        if (A_PRESSED(buttons2)) {
            paddle2y += paddleaspeed;
        } else {
            paddle2y += paddlespeed;
        }
    }

    // Check Collision
    if (ballx < LEFTWALL) {
        if (bally + 8 < paddle1y || bally - 24 > paddle1y) {
            ++score2;
            bally = 0x80;
            ballspeedy = 2;
            ballspeedx = 2;
            hitcount = 0;
            ballbasespeed = 1;
        } else {
            if (++hitcount == 5) {
                ++ballbasespeed;
                hitcount = 0;
            }
            if (bally < paddle1y) {
                ballup = 1;
                ballspeedy = ballbasespeed + 2;
                ballspeedx = ballbasespeed;
            } else if (bally - 16 > paddle1y) {
                ballup = 0;
                ballspeedy = ballbasespeed + 2;
                ballspeedx = ballbasespeed;
            } else {
                ballspeedy = ballbasespeed + 1;
                ballspeedx = ballspeedy;
            }
        }
    } else if (ballx > RIGHTWALL) {
        if (bally + 8 < paddle2y || bally - 24 > paddle2y) {
            ++score1;
            bally = 0x80;
            ballspeedy = 2;
            ballspeedx = 2;
            hitcount = 0;
            ballbasespeed = 1;
        } else {
            if (++hitcount == 5) {
                ++ballbasespeed;
                hitcount = 0;
            }
            if (bally < paddle2y) {
                ballup = 1;
                ballspeedy = ballbasespeed + 2;
                ballspeedx = ballbasespeed;
            } else if (bally - 16 > paddle2y) {
                ballup = 0;
                ballspeedy = ballbasespeed + 2;
                ballspeedx = ballbasespeed;
            } else {
                ballspeedy = ballbasespeed + 1;
                ballspeedx = ballspeedy;
            }
        }
    }
}
