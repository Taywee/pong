# Changelog

This catalogs notable changes by version.  Versions are in git tags as vx.y.z,
sort of following semantic versioning (though that makes little sense with an
NES game, it's useful for distinguishing major changes from minor ones).

## [0.0.0] - 2018-01-23

* initialized project