; vim: ft=asm_ca65
;
.segment "CHARS"
    .incbin "pong.chr" ; if you have one
.segment "HEADER"
    .byte "NES",$1A
    ; PRG ROM, 32KB
    .byte 2
    ; CHR ROM, 8 KB
    .byte 1
    ; Flags 6
    .byte %00000000
    ; Flags 7
    .byte %00000000
    ; PRG RAM
    .byte $01
.segment "VECTORS"
    .word nmi, reset, 0
.segment "RODATA"
palette:
    .byte $1D, $00, $10, $30
    .byte $1D, $00, $10, $30
    .byte $1D, $00, $10, $30
    .byte $1D, $00, $10, $30
    .byte $1D, $00, $10, $30
    .byte $1D, $00, $10, $30
    .byte $1D, $00, $10, $30
end_palette:
s_palette = (end_palette - palette)

.segment "OAM"
sprites:
_ball_sprite:
    .byte $80, $04, $00, $80

_player1_paddle_sprites:
    .byte $80, $01, $00, $00
    .byte $88, $02, $00, $00
    .byte $90, $03, $00, $00
_player2_paddle_sprites:
    .byte $80, $01, $00, $F8
    .byte $88, $02, $00, $F8
    .byte $90, $03, $00, $F8

.ZEROPAGE
_buttons1:
    .res 1  ; player 1 gamepad buttons, one bit per button
_buttons2:
    .res 1  ; player 2 gamepad buttons, one bit per button
tmp1:
    .res 1
tmp2:
    .res 1
tmp3:
    .res 1
tmp4:
    .res 1
ptr1:
    .res 2
ptr2:
    .res 2
ptr3:
    .res 2
ptr4:
    .res 2

.segment "INITZP": zeropage
;; DECLARE SOME CONSTANTS HERE
STATETITLE     = $00  ; displaying title screen
STATEPLAYING   = $01  ; move paddles/ball, check for collisions
STATEGAMEOVER  = $02  ; displaying game over screen

_ballup:
    .byte $01
_ballright:
    .byte $01
_ballbasespeed:
    .byte $01
nmiran:
    .byte $00
_hitcount:
    .byte $00
_bally:
    .byte $50
_ballx:
    .byte $80
_paddle1y:
    .byte $40
_paddle2y:
    .byte $40
_ballspeedx:
    .byte $02
_ballspeedy:
    .byte $02
_paddlespeed:
    .byte $02
_paddleaspeed:
    .byte $04
gamestate:
    .byte STATEPLAYING
_score1:
    .byte 0  ; player 1 score, 0-15
_score2:
    .byte 0  ; player 2 score, 0-15

  
.segment "CODE"

.import PPUCTRL, PPUMASK, PPUSTATUS, OAMADDR, OAMDATA, PPUSCROLL, PPUADDR, PPUDATA, OAMDMA, CONTROLLER1, CONTROLLER2
.import __OAM_SIZE__, __OAM_RUN__, __OAM_LOAD__
.import __INITZP_SIZE__, __INITZP_RUN__, __INITZP_LOAD__

.exportzp _hitcount, _ballbasespeed, _ballright, _ballx, _ballspeedx, _bally, _ballspeedy, _ballup, _paddle1y, _buttons1, _paddlespeed, _paddleaspeed, _score1, _score2, _paddle2y, _buttons2, tmp1, tmp2, tmp3, tmp4, ptr1, ptr2, ptr3, ptr4
.export _ball_sprite, _player1_paddle_sprites, _player2_paddle_sprites

.import _EnginePlaying, _UpdateSprites, _DrawScore

.include "memcpy.asm"

vblankwait:
    bit PPUSTATUS
    bpl vblankwait
    rts

reset:
    sei          ; disable IRQs
    cld          ; disable decimal mode
    ldx #$40
    stx $4017    ; disable APU frame IRQ
    ldx #$FF
    txs          ; Set up stack
    inx          ; now X = 0
    stx PPUCTRL    ; disable NMI
    stx PPUMASK    ; disable rendering
    stx $4010    ; disable DMC IRQs

    jsr vblankwait

clrmem:
    lda #$00
    sta $0000, x
    sta $0100, x
    sta $0200, x
    sta $0300, x
    sta $0400, x
    sta $0500, x
    sta $0600, x
    sta $0700, x
    inx
    bne clrmem
   
    jsr vblankwait

loadpalettes:
    lda PPUSTATUS             ; read PPU status to reset the high/low latch
    lda #$3f
    sta PPUADDR             ; write the high byte of $3F00 address
    lda #$00
    sta PPUADDR             ; write the low byte of $3F00 address
    ldx #$00              ; start out at 0
@loop:
    lda palette, x        ; load data from address (palette + the value in x)
                            ; 1st time through loop it will load palette+0
                            ; 2nd time through loop it will load palette+1
                            ; 3rd time through loop it will load palette+2
                            ; etc
    sta PPUDATA             ; write to PPU
    inx                   ; X = X + 1
    cpx #s_palette              ; compare x to hex $10, decimal 16 - copying 16 bytes = 4 sprites
    bne @loop  ; Branch to LoadPalettesLoop if compare was Not Equal to zero
                          ; if compare was equal to 32, keep going down
                          
loadsprites:
    lda #>__OAM_SIZE__
    tay
    lda #<__OAM_SIZE__
    tax

    memcpy __OAM_RUN__, __OAM_LOAD__

;;;Set some initial ball stats
; TODO: switch this to a ROM->RAM push instead of defining in-code
;
    lda #>__INITZP_SIZE__
    tay
    lda #<__INITZP_SIZE__
    tax
    memcpy __INITZP_RUN__, __INITZP_LOAD__

    lda #%10000000   ; enable NMI, sprites from Pattern Table 0, background from Pattern Table 1
    sta PPUCTRL

    lda #%00011110   ; enable sprites, enable background, no clipping on left side
    sta PPUMASK

forever:
    lda nmiran
    beq forever     ;jump back to Forever, infinite loop, waiting for NMI
    lda #0
    sta nmiran

    jsr ReadController1
    jsr ReadController2

GameEngine:  
    LDA gamestate
    CMP #STATETITLE
    bne :+    ;;game is displaying title screen
    jsr EngineTitle
    jmp @done
:
      
    LDA gamestate
    CMP #STATEGAMEOVER
    bne :+
    jsr EngineGameOver  ;;game is displaying ending screen
    jmp @done
:
    
    LDA gamestate
    CMP #STATEPLAYING
    bne @done
    jsr _EnginePlaying   ;;game is playing
@done:  
  
    jsr _UpdateSprites  ;;set ball/paddle sprites from positions
    jmp forever

nmi:
    ; push processor status, A, X, and Y
    php
    pha
    txa
    pha
    tya
    pha

    lda #$01
    sta nmiran

    lda #$00
    sta OAMADDR       ; set the low byte (00) of the RAM address
    lda #>sprites
    sta OAMDMA       ; set the high byte (02) of the RAM address, start the transfer

    jsr _DrawScore

    ;;this is the PPU clean up section, so rendering the next frame starts properly.
    lda #%10010000   ; enable NMI, sprites from Pattern Table 0, background from Pattern Table 1
    sta PPUCTRL
    lda #%00011110   ; enable sprites, enable background, no clipping on left side
    sta PPUMASK
    lda #$00        ;;tell the ppu there is no background scrolling
    sta PPUSCROLL
    sta PPUSCROLL


    ; pop Y, X, A, and Proocessor Status
    pla
    tay
    pla
    tax
    pla
    plp
    rti
      
 
;;;;;;;;
 
EngineTitle:
  ;;if start button pressed
  ;;  turn screen off
  ;;  load game screen
  ;;  set starting paddle/ball position
  ;;  go to Playing State
  ;;  turn screen on
  rts

;;;;;;;;; 
 
EngineGameOver:
  ;;if start button pressed
  ;;  turn screen off
  ;;  load title screen
  ;;  go to Title State
  ;;  turn screen on 
  rts
 
.macro ReadController addr, buttons
    lda #$01
    sta CONTROLLER1
    sta buttons
    lsr A
    sta CONTROLLER1
@loop:
    lda addr
    lsr A            ; bit0 -> Carry
    rol buttons     ; bit0 <- Carry
    bcc @loop
    rts
.endmacro

ReadController1:
    ReadController CONTROLLER1, _buttons1

ReadController2:
    ReadController CONTROLLER2, _buttons2
