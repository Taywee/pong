ASFLAGS += -I /usr/share/cc65/asminc
CFLAGS += -O -Os -Oi

CC = cc65
AS = ca65
LD = ld65

ASSEMBLE = $(AS) $(ASFLAGS)
COMPILE = $(CC) $(CFLAGS)
LINK = $(LD) $(LDFLAGS)

mkfile_path := $(abspath $(lastword $(MAKEFILE_LIST)))
dirname := $(basename $(notdir $(patsubst %/,%,$(dir $(mkfile_path)))))

ASM_SOURCES = pong.asm
C_SOURCES = $(wildcard *.c)
S_SOURCES = $(C_SOURCES:.c=.s)
OBJECTS =  $(ASM_SOURCES:.asm=.o) $(S_SOURCES:.s=.o)

EXECUTABLE = $(dirname).nes

.PHONY: all clean

all: $(EXECUTABLE)
clean:
	-rm -v $(OBJECTS) $(EXECUTABLE)

$(EXECUTABLE): nes.cfg $(OBJECTS)
	$(LINK) -o$@ -C$^

pong.o : pong.asm pong.chr memcpy.asm
	$(ASSEMBLE) -o$@ $<

playing.o : playing.s
	$(ASSEMBLE) -o$@ $<

playing.s : playing.c
	$(COMPILE) -o$@ $<

render.o : render.s
	$(ASSEMBLE) -o$@ $<

render.s : render.c
	$(COMPILE) -o$@ $<
