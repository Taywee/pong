#ifndef REGISTERS_H
#define REGISTERS_H

#define PPUCTRL 0x2000
#define PPUMASK 0x2001
#define PPUSTATUS 0x2002
#define OAMADDR 0x2003
#define OAMDATA 0x2004
#define PPUSCROLL 0x2005
#define PPUADDR 0x2006
#define PPUDATA 0x2007
#define OAMDMA 0x4014
#define CONTROLLER1 0x4016
#define CONTROLLER2 0x4017
#define SET(addr, value) *(char*)addr = value

#endif /* ifndef REGISTERS_H */
