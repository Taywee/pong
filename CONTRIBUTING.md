* Don't screw it up
* Keep with the coding style
* Code should be well-commented, but not overcommented
* The most valuable changes are ones that don't change behavior, but improve
  clarity through refactoring, better macro use, better comments, etc.
* Please use the issue tracker.  If there is a task you want to tackle, put an
  issueup for it, and make it clear that you are tackling it, and make a pull
  request that can be linked to the issue.  I'm okay with a pull request without
  an issue, but I'd prefer an issue to be present.